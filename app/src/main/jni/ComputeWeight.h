//
// Created by zigio on 27/05/18.
//

#ifndef ESP_PROJECT_COMPUTEWEIGHT_H
#define ESP_PROJECT_COMPUTEWEIGHT_H


#include <opencv2/core/mat.hpp>

class ComputeWeight {

public:
    static std::vector<cv::Mat> computeWeight(std::vector<cv::Mat> images,double contrastParam,double saturationParam,double exposionParam);
private:
    static std::vector<cv::Mat> computeLaplacian(std::vector<cv::Mat> images);
    static std::vector<cv::Mat> computeSaturation(std::vector<cv::Mat> images);
    static std::vector<cv::Mat> computeExposed(std::vector<cv::Mat> images);
};


#endif
