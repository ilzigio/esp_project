//
// Created by zigio on 27/05/18.
//

#ifndef ESP_PROJECT_PYRAMID_H
#define ESP_PROJECT_PYRAMID_H


#include <vector>
#include <opencv2/core/mat.hpp>

class Pyramid {
public:
    static cv::Mat computePyramid(std::vector<cv::Mat> img,std::vector<cv::Mat> weight);
};


#endif
