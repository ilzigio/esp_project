//
// Created by zigio on 27/05/18.
//

#include <opencv2/core.hpp>
#include <opencv/cv.hpp>
#include <iostream>
#include <fstream>
#include <android/log.h>
#include "ComputeWeight.h"

using namespace cv;
using namespace std;

#define TAG "HDR-project-native"

/**
 * This method is used to compute the weight matrix for every burst
 * @param images is the vector of image, that represents the burst
 * @param contrastParam is the parameter used to weight the contrast matrix
 * @param saturationParam is the parameter used to weight the saturation matrix
 * @param exposionParam is the parameter used to weight the exposure matrix
 * @return vector of mat that includes all the weight matrixes
 */
std::vector<cv::Mat>
ComputeWeight::computeWeight(std::vector<cv::Mat> images, double contrastParam,
                             double saturationParam,
                             double exposionParam) {


    __android_log_print(ANDROID_LOG_INFO, TAG, "****COMPUTE WEIGHT*****");

    vector<Mat> lapl = computeLaplacian(images);
    vector<Mat> contr = computeSaturation(images);
    vector<Mat> expo = computeExposed(images);


    Mat sumWeigh(images.at(0).rows, images.at(0).cols, CV_32F, Scalar::all(0));

    int channels = images[0].channels();
    CV_Assert(channels == 1 || channels == 3);
    Size size = images[0].size();


    vector<Mat> weights(images.size());
    Mat weight_sum = Mat::zeros(size, CV_32F);

    //for every image are computed the final weight matrix
    for (int i = 0; i < images.size(); i++) {


        if (contrastParam > 0) {
            pow(lapl.at(i), contrastParam, lapl.at(i));
            weights[i] = lapl.at(i);
        }
        if (saturationParam > 0) {
            pow(contr.at(i), saturationParam, contr.at(i));
            if (weights[i].empty()) weights[i] = contr[i];
            else weights[i] = weights[i].mul(contr[i]);
        }
        if (exposionParam > 0) {
            pow(expo.at(i), exposionParam, expo.at(i));
            if (weights[i].empty()) weights[i] = expo[i];
            else weights[i] = weights[i].mul(expo[i]);
        }

        weights[i] = weights[i] + 1e-12f;
        weight_sum = weight_sum + weights[i];

    }


    //in this line the weights normalization are executed
    for (int i = 0; i < images.size(); i++) {
        weights.at(i) = weights.at(i) / weight_sum;
    }

    return weights;

}


/**
 * This method computed the laplacian filter and subsequently the absolute value for the burst
 * @param images the vector of image that represents the burst
 * @return the vector of response image
 */

std::vector<cv::Mat> ComputeWeight::computeLaplacian(std::vector<cv::Mat> images) {
    vector<Mat> exit(images.size());
    for (int i = 0; i < images.size(); ++i) {
        Mat img, gray, contrast;
        img = images.at(i).clone();
        cvtColor(img, gray, COLOR_RGB2GRAY);
        Laplacian(gray, contrast, CV_32F);
        contrast = abs(contrast);
        exit.at(i) = contrast;
    }
    return exit;
}

/**
 * This method computes the saturation matrix for every image in the burst
 * @param images is the burst of image
 * @return return the vector of Mat with the saturation value
 */
std::vector<cv::Mat> ComputeWeight::computeSaturation(std::vector<cv::Mat> images) {
    vector<Mat> exit(images.size());
    //in order to compute the saturation, for every image first are computed the mean between the three colors value for each pixel,
    //and after are computed the standard devitation.
    for (int i = 0; i < images.size(); i++) {
        Mat bgr[3];
        split(images.at(i), bgr);

        Mat mean = Mat::zeros(images.at(i).size(), CV_32F);
        mean = mean + bgr[0] + bgr[1] + bgr[2];
        mean = mean / 3;


        Mat first = bgr[0] - mean;
        Mat second = bgr[1] - mean;
        Mat third = bgr[2] - mean;
        pow(first, 2.0f, first);
        pow(second, 2.0f, second);
        pow(third, 2.0f, third);
        Mat resultSat = Mat::zeros(images.at(0).size(), CV_32F);
        resultSat = resultSat + first + second + third;
        sqrt(resultSat, resultSat);
        exit.at(i) = resultSat;
    }
    return exit;
}

/**
 * This method computes the matrix of exposure for every image of the burst
 * @param images is the burst
 * @return the computer vector of matrix with the exposure value
 */
std::vector<cv::Mat> ComputeWeight::computeExposed(std::vector<cv::Mat> images) {
    vector<Mat> exit(images.size());
    //in order to compute these value, each pixel are weight with an gaussian of mean 0.5 and variance 0.2
    for (int i = 0; i < images.size(); i++) {
        Mat bgr[3];
        Mat tmp;
        images.at(i).convertTo(tmp, CV_32FC3);
        split(tmp, bgr);

        Mat first = bgr[0] - 0.5f;
        Mat second = bgr[1] - 0.5f;
        Mat third = bgr[2] - 0.5f;

        pow(first, 2.0f, first);
        pow(second, 2.0f, second);
        pow(third, 2.0f, third);

        first = -first / 0.08f;
        second = -second / 0.08f;
        third = -third / 0.08f;

        exp(first, first);
        exp(second, second);
        exp(third, third);

        exit.at(i) = first.mul(second).mul(third);
    }

    return exit;
}