//
// Created by zigio on 27/05/18.
//

#include <iostream>
#include <opencv2/imgproc.hpp>
#include <android/log.h>
#include "Pyramid.h"

#define TAG "HDR-project-native"


/**
 * This method using the gaussian and the laplacian pyramid computes the final image
 * @param img is the vector of mat that represent burst
 * @param weight is the vector of weight matrixes
 * @return the mat that contains the final image
 */
cv::Mat Pyramid::computePyramid(std::vector<cv::Mat> img,std::vector<cv::Mat> weight) {
    int minSize=cv::min(img.at(0).cols,img.at(0).rows);
    int maxlevel=(int)log(minSize)/log(2.0f);

    __android_log_print(ANDROID_LOG_INFO, TAG, "MAX LEVEL: %d", maxlevel);

    std::vector<cv::Mat> resultedPyramid(maxlevel + 1);

    //for every image the algorith builds the pyramid for the image and the weight
    for (int i = 0; i < img.size(); i++) {
        std::vector<cv::Mat> imagePyramid;
        std::vector<cv::Mat> weightPyramid;
        buildPyramid(img.at(i), imagePyramid, maxlevel);
        buildPyramid(weight.at(i), weightPyramid, maxlevel);

        //for the image the pyramid is built subtraction each level with the resized previous level
        for (int level = 0; level < maxlevel; level++) {
            cv::Mat tmp;
            cv::pyrUp(imagePyramid[level + 1], tmp, imagePyramid[level].size());
            imagePyramid[level] = imagePyramid[level] - tmp;
        }

        //in this for cycle are computed the resulted image for every image, and for every new image the resulted image are updated
        for (int level = 0; level <= maxlevel; level++) {
            std::vector<cv::Mat> splitted(3);
            cv::split(imagePyramid[level], splitted);

            splitted[0] = splitted[0].mul(weightPyramid[level]);
            splitted[1] = splitted[1].mul(weightPyramid[level]);
            splitted[2] = splitted[2].mul(weightPyramid[level]);

            cv::merge(splitted, imagePyramid[level]);

            if (resultedPyramid[level].empty()) {
                resultedPyramid[level] = imagePyramid[level];
            } else {
                resultedPyramid[level] = resultedPyramid[level] + imagePyramid[level];
            }
        }

        __android_log_print(ANDROID_LOG_INFO, TAG, "Giro image: %d", i);

    }


    //this for loop is used to built the final resulted image going up the resulted pyramid
    for (int level = maxlevel; level > 0; level--) {

        __android_log_print(ANDROID_LOG_INFO, TAG, "Giro di level: %d", level);

        cv::Mat tmp;
        pyrUp(resultedPyramid[level],tmp, resultedPyramid[level - 1].size());
        resultedPyramid[level - 1] = resultedPyramid[level - 1] + tmp;
    }

    return resultedPyramid[0];
}


