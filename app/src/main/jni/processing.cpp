#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/opencv.hpp>
#include <jni.h>
#include <android/log.h>
#include <string>
#include <opencv2/core/cvstd.hpp>
#include <opencv/cxmisc.h>
#include <iostream>
#include "ComputeWeight.h"
#include "Pyramid.h"

#define TAG "HDR-project-native"

using namespace std;
using namespace cv;


const int MAX_FEATURES = 500;
const float GOOD_MATCH_PERCENT = 0.15f;

/**
 * This method is used to align an image w.r.t a reference Image.
 * Now it is not used because have low performance.
 * @param imageToAlign   is the pointer to the image to align
 * @param referenceImage  is the pointer to the reference image
 * @param outputImage is the aligned image
 */

extern void alignImageFeature(Mat &imageToAlign, Mat &referenceImage, Mat &outputImage) {

    // Convert images to grayscale
    Mat im1Gray, im2Gray;
    cvtColor(imageToAlign, im1Gray, CV_BGR2GRAY);
    cvtColor(referenceImage, im2Gray, CV_BGR2GRAY);

    // Variables to store keypoints and descriptors
    std::vector<KeyPoint> keypoints1, keypoints2;
    Mat descriptors1, descriptors2;

    // Detect ORB features and compute descriptors.
    Ptr<Feature2D> orb = ORB::create(MAX_FEATURES);
    orb->detectAndCompute(im1Gray, Mat(), keypoints1, descriptors1);
    orb->detectAndCompute(im2Gray, Mat(), keypoints2, descriptors2);

    // Match features.
    std::vector<DMatch> matches;
    Ptr<DescriptorMatcher> matcher = DescriptorMatcher::create("BruteForce-Hamming");
    matcher->match(descriptors1, descriptors2, matches, Mat());

    // Sort matches by score
    std::sort(matches.begin(), matches.end());

    // Remove not so good matches
    const int numGoodMatches = static_cast<const int>(matches.size() * GOOD_MATCH_PERCENT);
    matches.erase(matches.begin() + numGoodMatches, matches.end());


    // Draw top matches
    //Mat imMatches;
    //drawMatches(imageToAlign, keypoints1, referenceImage, keypoints2, matches, imMatches);

    // Extract location of good matches
    std::vector<Point2f> points1, points2;

    for (size_t i = 0; i < matches.size(); i++) {
        points1.push_back(keypoints1[matches[i].queryIdx].pt);
        points2.push_back(keypoints2[matches[i].trainIdx].pt);
    }

    // Find homography
    Mat h = findHomography(points1, points2, RANSAC);

    // Use homography to warp image
    warpPerspective(imageToAlign, outputImage, h, referenceImage.size());

}

/**
 * This method is used to computing the algorithm and to return the image to the android part
 * @param input is the vector of image that represents the burst
 * @param output is the final image after the computation
 */
extern void processingImage(vector<Mat> &input, Mat &output) {

    vector<Mat> inputImages = input;
    Mat &outputImage = output;

    __android_log_print(ANDROID_LOG_INFO, TAG, "Images founded in Native: %d",
                        (int) inputImages.size());

    //These parameters are used in the computation of the weight
    double const CONTRAST_PARAM = 1;
    double const SATURATION_PARAM = 1;
    double const EXPOSION_PARAM = 1;

    vector<Mat> img;

    if (inputImages.size() > 0) {
        __android_log_print(ANDROID_LOG_INFO, TAG, "Computing HDR image");

        for (int i = 0; i < input.size(); ++i) {
            Mat tmp = inputImages.at(i);

            __android_log_print(ANDROID_LOG_INFO, TAG, "Image: %d  -  with size: %d x %d", i,
                                tmp.rows, tmp.cols);

            //This method is used to convert the Mat to the format CV_32F, in order to have floating value
            tmp.convertTo(tmp, CV_32F, 1.0f / 255.0f);

            img.push_back(tmp);
        }


        //this method computes and returns the weight matrix of the burst
        vector<Mat> weight = ComputeWeight::computeWeight(img, CONTRAST_PARAM, SATURATION_PARAM,
                                                          EXPOSION_PARAM);

        //this method takes the burst and the weight and computes the final result
        Mat result = Pyramid::computePyramid(img, weight);


        __android_log_print(ANDROID_LOG_INFO, TAG,
                            "Image HDR saved in: /storage/emulated/0/Pictures/HDR_ESP/risultatoFinale1.jpg ");


        Mat resConvert;
        Mat secondConvert;

        //This method converts the floating point mat to the unsigned type , in order the mat with the correct type to the
        //android part
        result.convertTo(resConvert, CV_8UC3, 255);


        //This method converts the mat from BGR to RGB
        cvtColor(resConvert, resConvert, COLOR_BGR2RGB);
        outputImage = resConvert.clone();
    } else {
        __android_log_print(ANDROID_LOG_INFO,
                            TAG, "!!!!!!!!!ERROR IN PROCESSING FILE");

    }


}

/**
 * This method loads the image from the storage
 * @param env is the pointer to the JNI interfaceNoIn
 * @param adressPath_ is the path of the first burst
 * @param outputImagePath is the path of the output image
 * @param executeAlign indicates if computing the alignment or not
 */
extern void
extractImages(JNIEnv *env, jstring adressPath_, jlong outputImagePath, jboolean executeAlign) {


    Mat &outputImage = *(Mat *) outputImagePath;

    const char *adressPath = env->GetStringUTFChars(adressPath_, 0);
    String pathInput(adressPath);

    vector<String> pathImages;

    String pathGeneral = pathInput.substr(0, pathInput.length() - 5) + "*";
    glob(pathGeneral, pathImages);

    vector<Mat> images;

    for (size_t i = 0; i < pathImages.size(); i++) {
        Mat temp = imread(pathImages.at(i), CV_32FC3);
        images.push_back(temp);
    }
    env->ReleaseStringUTFChars(adressPath_, adressPath);

    // Execute alignment if requested
    if (executeAlign) {
        // alignImage(temp, images[0], temp);
        // Align input images
        Ptr<AlignMTB> alignMTB = createAlignMTB();
        alignMTB->process(images, images);
    }
    __android_log_print(ANDROID_LOG_INFO, TAG, "\nImages founded in Native: %d",
                        (int) images.size());


    processingImage(images, outputImage);
}

/**
 * This method is called from the Android application when the computational must started
 */

extern "C"
JNIEXPORT void JNICALL
Java_com_ziggiotto_daldegan_biasetton_hdr_1esp_VisualImageActivity_modifyImages(JNIEnv *env,
                                                                                jobject instance,
                                                                                jstring adressPath_,
                                                                                jlong outputImagePath,
                                                                                jboolean executeAlignment) {
    extractImages(env, adressPath_, outputImagePath, executeAlignment);
}