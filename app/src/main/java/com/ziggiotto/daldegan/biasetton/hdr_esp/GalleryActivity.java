package com.ziggiotto.daldegan.biasetton.hdr_esp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.NumberPicker;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;

import com.bumptech.glide.Glide;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;



public class GalleryActivity extends AppCompatActivity {
    private int desired_val;
    private GridView mGridView;
    private ArrayList<String> images;
    private GalleryAdapter gridAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gallery_activity);
        setupActionBar();

        // Obtain saved preferences
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        desired_val = sharedPref.getInt(getString(R.string.gridview_columns_number), 4);

        // Populate the UI
        images = getListOfFiles();
        mGridView = findViewById(R.id.gridView);
        mGridView.setNumColumns(desired_val);
        gridAdapter = new GalleryAdapter(this, images);
        mGridView.setAdapter(gridAdapter);

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                showAlertDialog(images.get(position));
            }
        });

        mGridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                    gridAdapter.notifyDataSetChanged();
                }
            }

            @Override

            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.gallery_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.select_grid_width:
                showNumberPickerDialog();
                return true;
            case R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * Return a list of the files contained in the folder specified by the constant "burst_folder
     *
     * @return List of files contained in the specified folder
     */
    private ArrayList<String> getListOfFiles() {
        File dir = new File(Environment.getExternalStorageDirectory() + getString(R.string.burst_folder));
        Queue<File> files = new LinkedList<>();
        ArrayList<String> inFiles = new ArrayList<>();
        if (dir.listFiles() != null) {
            files.addAll(Arrays.asList(dir.listFiles()));
        }
        while (!files.isEmpty()) {
            File file = files.remove();
            if (file.isDirectory()) {
                files.addAll(Arrays.asList(file.listFiles()));
            } else {
                inFiles.add(file.getAbsolutePath());
            }
        }

        //Select only one image of a burst of N photos
        ArrayList<String> selectedImages = new ArrayList<>();

        for (int i = 0; i < inFiles.size(); i++) {
            String pathFile = inFiles.get(i);
            String generalPath = pathFile.substring(0, pathFile.length() - 5);

            boolean found = false;
            for (int j = 0; j < selectedImages.size() && !found; j++) {
                String selected = selectedImages.get(j);
                if (selected.substring(0, selected.length() - 5).equalsIgnoreCase(generalPath)) {
                    found = true;
                }
            }

            //Extract the number of the image of the burst
            if (!found && pathFile.charAt(pathFile.length() - 5) == '2') {
                selectedImages.add(pathFile);
            }
        }

        return selectedImages;
    }

    /**
     * Send back results to CameraActivity and close the current Activity
     *
     * @param selected_image path of the selected image
     */
    private void startAlgoritm(String selected_image) {
        Intent visualImage = new Intent(getBaseContext(), VisualImageActivity.class);
        visualImage.putExtra("burstPath", selected_image);
        visualImage.putExtra("isLaunchedFromCamera", false);
        startActivity(visualImage);
        finish();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!OpenCVLoader.initDebug()) {
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_4_0, getBaseContext(), mLoaderCallBack);
        } else {
            mLoaderCallBack.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    private BaseLoaderCallback mLoaderCallBack = new BaseLoaderCallback(getBaseContext()) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case SUCCESS: {
                    // Load native method
                    System.loadLibrary("processing");
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };


    /**
     * Create and show a dialog to pick the desired size of the grid
     */
    private void showNumberPickerDialog() {
        final AlertDialog.Builder d = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.number_picker_layout, null);
        d.setTitle(R.string.number_picker_dialog_title);
        d.setMessage(R.string.number_picker_dialog_message);
        d.setView(dialogView);
        final NumberPicker numberPicker = dialogView.findViewById(R.id.dialog_number_picker);
        numberPicker.setMaxValue(6);
        numberPicker.setMinValue(1);
        numberPicker.setValue(desired_val);
        numberPicker.setWrapSelectorWheel(false);
        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int oldVal, int newVal) {
                desired_val = newVal;
            }
        });
        d.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Update the UI
                mGridView.setNumColumns(desired_val);
                mGridView.invalidateViews();
                gridAdapter = new GalleryAdapter(GalleryActivity.this, images);
                mGridView.setAdapter(gridAdapter);

                // Save the choice to Shared Preferences
                SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt(getString(R.string.gridview_columns_number), desired_val);
                editor.apply();
            }
        });
        d.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        AlertDialog alertDialog = d.create();
        alertDialog.show();
    }

    /**
     * Create a Dialog to visualize the selected burst
     *
     * @param imagePath path of the selected image
     */
    private void showAlertDialog(final String imagePath) {
        LayoutInflater inflater = this.getLayoutInflater();
        final ArrayList<String> burstFiles = getBurst(imagePath);

        View dialogView = inflater.inflate(R.layout.burst_dialog, null);
        GridView gridView = dialogView.findViewById(R.id.gridView_dialog);
        DialogAdapter dialogAdapter = new DialogAdapter(this, burstFiles);
        gridView.setAdapter(dialogAdapter);


        // Set grid view to alertDialog
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        builder.setTitle(getString(R.string.selected_burst));
        builder.setPositiveButton(getString(R.string.select), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startAlgoritm(imagePath);
            }
        });
        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // close
            }
        });
        builder.setNeutralButton(getString(R.string.delete_burst), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new AlertDialog.Builder(GalleryActivity.this)
                        .setTitle(getString(R.string.delete_burst))
                        .setMessage(getString(R.string.delete_burst_confirm))
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                images.remove(imagePath);
                                deleteBurst(burstFiles);
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        }).show();

            }
        });
        builder.show();

    }

    /**
     * Delete the selected burst of photo
     *
     * @param imagePath path of one image og the burst
     */
    private void deleteBurst(ArrayList<String> imagePath) {
        for (String filePath : imagePath) {
            File file = new File(filePath);
            boolean deleted = file.delete();
        }
        gridAdapter.notifyDataSetChanged();
    }

    /**
     * Get all the image's paths of a burst of photo
     *
     * @param imagePath path of one image of the burst
     * @return list of image's paths belonging to the burst
     */
    private ArrayList<String> getBurst(String imagePath) {
        String pathGeneral = imagePath.substring(0, imagePath.length() - 5);
        File dir = new File(Environment.getExternalStorageDirectory() + getString(R.string.burst_folder));
        Queue<File> files = new LinkedList<>();
        ArrayList<String> inFiles = new ArrayList<>();
        if (dir.listFiles() != null) {
            files.addAll(Arrays.asList(dir.listFiles()));
        }
        while (!files.isEmpty()) {
            File file = files.remove();
            if (file.isDirectory()) {
                files.addAll(Arrays.asList(file.listFiles()));
            } else if (file.getAbsolutePath().contains(pathGeneral)) {
                inFiles.add(file.getAbsolutePath());
            }
        }
        Collections.sort(inFiles);
        return inFiles;
    }

    /**
     * Private class used as adapter for the GridView
     */
    private class GalleryAdapter extends BaseAdapter {

        private Context context;
        private ArrayList<String> img;


        GalleryAdapter(@NonNull Context context, ArrayList<String> img) {
            this.context = context;
            this.img = img;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView view = (ImageView) convertView;

            if (view == null) {
                view = new ImageView(context);
                view.setPadding(5, 5, 5, 5);
                //view.setScaleType(ImageView.ScaleType.FIT_XY);
                view.setAdjustViewBounds(true);

            }


            // Trigger the download of the URL asynchronously into the image view.
            Glide.with(context) //
                    .load(img.get(position))
                    .into(view);


            return view;
        }

        @Override
        public int getCount() {
            return images.size();
        }

        @Override
        public String getItem(int position) {
            return images.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
    }

    /**
     * Private class used as adapter for the Dialog
     */
    private class DialogAdapter extends BaseAdapter {

        Context mContext;
        ArrayList<String> listFiles;

        DialogAdapter(Context context, ArrayList<String> files) {
            this.mContext = context;
            this.listFiles = files;
        }

        @Override
        public int getCount() {
            return listFiles.size();
        }

        @Override
        public Object getItem(int position) {
            return listFiles.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.my_grid, parent, false);
            }

            ImageView temp = (ImageView) convertView;
            Glide.with(mContext)
                    .load(listFiles.get(position))
                    .into(temp);
            return convertView;
        }
    }
}
