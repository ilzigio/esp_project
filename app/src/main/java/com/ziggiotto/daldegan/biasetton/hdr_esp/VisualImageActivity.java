package com.ziggiotto.daldegan.biasetton.hdr_esp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvException;
import org.opencv.core.Mat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class VisualImageActivity extends AppCompatActivity {

    private ImageButton mPrevious;
    private ImageButton sharePhoto;
    private ProgressBar progress;
    private FrameLayout container;
    private boolean executeAlignment;
    private String burstPath;
    private File finalImage;
    private TextView text;
    private boolean saveBurst;
    private boolean isLaunchedFromCamera;
    private String timestamp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visual_image);

        // Obtain saved preferences
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        executeAlignment = prefs.getBoolean("execute_alignment_alg", true);
        saveBurst = prefs.getBoolean("save_burst", true);

        burstPath = getIntent().getStringExtra("burstPath");
        timestamp = burstPath.substring(burstPath.lastIndexOf("/") + 1, burstPath.length() - 6);
        isLaunchedFromCamera = getIntent().getBooleanExtra("isLaunchedFromCamera", false);

        mPrevious = findViewById(R.id.backToInit);
        sharePhoto = findViewById(R.id.sharePhoto);
        progress = findViewById(R.id.waitProgressBar);
        container = findViewById(R.id.container);
        text = findViewById(R.id.waitTextView);


        mPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(VisualImageActivity.this, CameraActivity.class));
                finish();
            }
        });

        sharePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                    StrictMode.setVmPolicy(builder.build());

                    MimeTypeMap mime = MimeTypeMap.getSingleton();

                    String ext = finalImage.getName().substring(finalImage.getName().lastIndexOf(".") + 1);
                    String type = mime.getMimeTypeFromExtension(ext);

                    Intent sharingIntent = new Intent("android.intent.action.SEND");
                    sharingIntent.setType(type);
                    sharingIntent.putExtra("android.intent.extra.STREAM", Uri.fromFile(finalImage));
                    startActivity(Intent.createChooser(sharingIntent, "Share using"));
                } catch (Exception e) {
                    Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        // Execute algorithm if not already executed
        File sdCard = Environment.getExternalStorageDirectory();
        String path = sdCard.getAbsolutePath() + getString(R.string.app_folder);
        String outputFile = path + "/" + timestamp + ".jpg";
        File temp = new File(outputFile);
        if (!temp.exists()) {
            new AlgorithmExecution().execute(burstPath);
        } else {
            displayResult(outputFile);
        }
    }

    public void displayResult(String pathReadComplete) {
        finalImage = new File(pathReadComplete);

        ImageView viewImage = findViewById(R.id.imageView);

        Glide.with(this)
                .load(finalImage)
                .into(viewImage);
    }

    @Override
    public void onResume() {
        super.onResume();


        if (!OpenCVLoader.initDebug()) {
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_4_0, this, mLoaderCallBack);
        } else {
            mLoaderCallBack.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }


    @Override
    public void onPause() {

        super.onPause();
    }

    private BaseLoaderCallback mLoaderCallBack = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case SUCCESS: {
                    // Load native method
                    System.loadLibrary("processing");
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    /**
     * Save the final image to disk
     *
     * @param outputImage the image to save in Mat format
     * @param filename    the name of the output image
     * @return path of the saved image
     */
    private String saveImage(Mat outputImage, String filename) {
        Bitmap bmp = null;
        try {
            bmp = Bitmap.createBitmap(outputImage.cols(), outputImage.rows(), Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(outputImage, bmp);
        } catch (CvException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        File sdCard = Environment.getExternalStorageDirectory();

        String path = sdCard.getAbsolutePath() + getString(R.string.app_folder);


        File temp = new File(path);

        boolean success = true;
        if (!temp.exists()) {
            success = temp.mkdir();
        }
        if (success) {

            File dest = new File(path, filename);

            try {
                out = new FileOutputStream(dest);
                bmp.compress(Bitmap.CompressFormat.JPEG, 100, out); // bmp is your Bitmap instance
                // PNG is a lossless format, the compression factor (100) is ignored

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        MediaScannerConnection.scanFile(this, new String[]{path + "/" + filename}, null, null);
        return path + "/" + filename;
    }

    /**
     * Lock or unlock programmatically the Activity Rotation
     *
     * @param value true for lock and false to unlock
     */
    public void lockDeviceRotation(boolean value) {
        if (value) {
            int currentOrientation = getResources().getConfiguration().orientation;
            if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
            } else {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
            }
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_USER);
        }
    }

    /**
     * Native method that execute the algorithm
     *
     * @param adressPath       path of the first image of the burst
     * @param outputImagePath  path of the output image
     * @param executeAlignment specifies if the algorithm has to execute an image align before computations
     */
    private native void modifyImages(String adressPath, long outputImagePath, boolean executeAlignment);

    /**
     * AsyncTask utilized to execute the algorithm in background
     */
    private class AlgorithmExecution extends AsyncTask<String, Void, Void> {
        String pathSaverComplete;

        @Override
        protected void onPreExecute() {
            lockDeviceRotation(true);
            progress.setVisibility(View.VISIBLE);
            container.setVisibility(View.INVISIBLE);
            text.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(String... params) {
            Mat outputImage = new Mat();
            modifyImages(params[0], outputImage.getNativeObjAddr(), executeAlignment);//call to native function
            pathSaverComplete = saveImage(outputImage, timestamp + ".jpg");

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            // Delete burst if requested
            if (!saveBurst && isLaunchedFromCamera) {
                File dir = new File(Environment.getExternalStorageDirectory() + getString(R.string.burst_folder));
                Queue<File> files = new LinkedList<>();
                if (dir.listFiles() != null) {
                    files.addAll(Arrays.asList(dir.listFiles()));
                }
                while (!files.isEmpty()) {
                    File file = files.remove();
                    if (file.isDirectory()) {
                        files.addAll(Arrays.asList(file.listFiles()));
                    } else if (file.getName().contains(timestamp)) {
                        boolean deleted = file.delete();
                    }
                }
            }
            displayResult(pathSaverComplete);
            progress.setVisibility(View.INVISIBLE);
            container.setVisibility(View.VISIBLE);
            text.setVisibility(View.INVISIBLE);
            lockDeviceRotation(false);
            super.onPostExecute(aVoid);
        }
    }
}
