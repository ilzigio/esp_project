# README #
Questa repository contiene il codice per il progetto finale del corso di Programmazione Sistemi Embedded, Università di Padova, 2017-2018.

Lo scopo di questo progetto è quello di sviluppare una applicazione Android per esegure delle foto di alta qualità utilizzando tecniche di multishot-photography.

### Autori ###
* Biasetton Matteo
* Dal Degan Daniele
* Ziggiotto Andrea

## Licenza ##
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.